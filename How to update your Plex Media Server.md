# How to update your Plex Media Server manually

## Requirements

-   WinSCP
-   WinRAR, 7zip or something else

## Step One - Download the latest version of Plex

Open the administration UI of your Plexserver.

Go to: Settings > General

an see if there is a new Update available. If so, download it.

After done so, unpack it somewhere on your desktop. In addition to that you have to rename the unpacked folder to **plexmediaserver**

## First Two - Enable SSH on your NAS

First log into your NAS and navigate to **Services**. Then scroll down until you see _SSH_. Enable it and click on the "Edit-Pen". Make sure that:

-   TCP-Port = 22
-   "Log in as Root with password" is enabled
-   "Password-Authentification" is enabled

Now save!

## Step Three - Connect to NAS via SSH using WinSCP

Open WinSCP.

As the _serveraddress_ you can use the IP-Address oder the hostname of your NAS. Now enter the credentials of the root-user and connect.

Click on the marked area!

![logo](sources/images/pmu-1.png)

Select "**/\<root>**"

Open the following path, where "dataset" stands for the dataset your plex plugin is installed:

> mnt/{dataset}/iocage/jails/plex/root/usr/local/share

Rename the existing **plexmediaserver** to something like **plexmediaserver-old**.
Copy the recently downloaded, unpacked folder into the folder of the path.

Do a right-click on the new folder and hit **Properties**. Make sure:

-   Group = wheel [0]
-   Owner = root [0]

Check the checkbox "Set group, owner and permissions recursively and submit.

Now switch into the new **plexmediaserver** folder.

Select a file called **Plex Media Server** and then click on the marked area on the image below.

![logo](sources/images/pmu-2.png)

Type in **Plex_Media_Server** and hit "OK".

Click **Shift + Ctrl + T** or simply click on the marked area of the image below.

![logo](sources/images/pmu-3.png)

Enter the following line.

```
chmod -h 775 Plex_Media_Server
```

Now its almost done. The next step is not neccessary.

With

```
ls -al *Media*
```

you can check that all permissions are set correctly

Now restart your NAS and: Boom! It should work.

### Important

If it does not work, you can always delete the **new** plexmediaserver-folder and rename the old one back to _plexmediaserver_. And then it should work properly again, but with the old version.

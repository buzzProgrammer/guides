# Rainbow Six Siege: Operation Throwback

`I do know, this is a lot of information down there, but it is worth reading. It consists of all neccessary informations you need to sucessfully install older versions of Siege.`

You are looking for a way to get that **Old Rainbow**-Feeling back. Well here we have one.

### Short Overview of the steps you need to do:

-   Join the Discord-Server (Links, News, Polls)
-   Download the files

### Seems easy! Right? Because it is!

First of all you need to download a driver, so the Programm (R6Downloader) can work properly. [Click here](https://dotnet.microsoft.com/download/dotnet/thank-you/sdk-3.1.300-windows-x64-installer) to download the driver. Then **install** the driver.

Secondly you need to join the following [Discord-Server](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqa2NRZ1FQVklsWnZfc3lJZHJraGlhaE5lVHlKUXxBQ3Jtc0tuQ1I2N1RZR3FFZzE0S1Z3VnNyNzJkMkRnUGNqcTQtaHBNYUprZm14RlhZYXgwa0otTkV0S0p6MVk1ZFZna1p2TVpXMlBKRnlxUGI5dS1nVlZwenZWaDJUT1ZYSWJyekhPRW1DcWhhQnM1M3lLS3pwOA&q=https%3A%2F%2Fdiscord.com%2Finvite%2FZKHsmTf). If you have done so switch to the Channel called "**game-downloads**". Look for the latest version of a program called "**R6Downloader.exe**" It is possible that the name varies but it probably contains the string _R6Downloader_ and is an _Executable File_ (.exe). It might cause your antivirus to scream, but it actually doesn't contain any viruses.

NOTE: Depending on changes, the Channel-Admins could make, the R6Downloader could also be a .rar or .zip. In that case, you will need to unpack that file. The password to unpack it is: **Throwback**

Afterwards you run the R6Downloader - Executable as an Administrator.

From then on, the further process should be obvious.

NOTE: You will need to enter your _Steam-Name_ as well as your _Steam-Password_. It is necessary, because the programm will check, if you actually own the game. If you wouldn't own the game, this would be illegal, so it won't work.

If there shows up a message, that your account is protected by _Steam Guard_, go to your Mail-Account. You will receive an E-Mail with a verification code. Copy the code and paste it in the Command Shell (Black Window).

After that the Version you selected will be downloaded from the Steam-Depot.
Usually it needs 30 Minutes up to 2 Hours, depending on the size.

Now go back to the discord server, in the channel **game-downloads** and look for a file starting with **PLAZA**. Download the latest one.

Create a folder on your Desktop and unpack the directory in it. Password again **Throwback**.

Drag&Drop thos files in your Y2S3_BloodOrchid (just an example) directory. A prompt will appear, asking you, if you want to override the files. **Cancel it**. Do the same again, but this time **don't** cancel it.

### Finally

Open the file called **CODEX**. Change the name after **UserName=** to a name you like (it will be your in-game name)

Now start the **RainbowSix**-File (not the RainbowSix_BE) file.

### You're good to go.

## Now really quick:

1. Install [driver](https://dotnet.microsoft.com/download/dotnet/thank-you/sdk-3.1.300-windows-x64-installer)
2. Join [Discord](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqa2NRZ1FQVklsWnZfc3lJZHJraGlhaE5lVHlKUXxBQ3Jtc0tuQ1I2N1RZR3FFZzE0S1Z3VnNyNzJkMkRnUGNqcTQtaHBNYUprZm14RlhZYXgwa0otTkV0S0p6MVk1ZFZna1p2TVpXMlBKRnlxUGI5dS1nVlZwenZWaDJUT1ZYSWJyekhPRW1DcWhhQnM1M3lLS3pwOA&q=https%3A%2F%2Fdiscord.com%2Finvite%2FZKHsmTf)
3. Download R6Downloader
4. Download latest version of PLAZA
5. Launch the R6Downloader as Administrator
6. Downloader the version you want
7. Copy "crack" files into your games version folder
8. Launch the RainbowSix-File

---

ATTENTION: I don't guarante, the functionality. Furthermore, I am not liable for any damage that may occur.

---

Helpful Links & Ressources

-   [YoutTube-Video](https://www.youtube.com/watch?v=4TFyQvkFiN4) by Content-Creator **Lnyx**
-   [Discord-Server](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqa2NRZ1FQVklsWnZfc3lJZHJraGlhaE5lVHlKUXxBQ3Jtc0tuQ1I2N1RZR3FFZzE0S1Z3VnNyNzJkMkRnUGNqcTQtaHBNYUprZm14RlhZYXgwa0otTkV0S0p6MVk1ZFZna1p2TVpXMlBKRnlxUGI5dS1nVlZwenZWaDJUT1ZYSWJyekhPRW1DcWhhQnM1M3lLS3pwOA&q=https%3A%2F%2Fdiscord.com%2Finvite%2FZKHsmTf)
-   [Driver](https://dotnet.microsoft.com/download/dotnet/thank-you/sdk-3.1.300-windows-x64-installer)
